# pdf-fact

[![GitLab CI](https://gitlab.com/arno750/pdf-fact/badges/main/pipeline.svg)](https://gitlab.com/arno750/pdf-fact/-/commits/main)

PDF invoice generator

## Support

[open an issue here](https://gitlab.com/arno750/pdf-fact/-/issues).

## Authors & contributors

Original setup of this repository by [Arnaud WIELAND](https://gitlab.com/arno750).

Based on a PDF generation tool by [Romain RICHARD](https://gitlab.com/romaiiiinnn)

## License

Every details about licence are in a [separate document](LICENSE).

## About the project

The format of the generated document is compliant with the rules applying on french invoice.

# Build

```bash
make
```
