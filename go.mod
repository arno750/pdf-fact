module pdf-fact

go 1.17

require github.com/signintech/gopdf v0.15.0

require github.com/magiconair/properties v1.8.5

require (
	github.com/phpdave11/gofpdi v1.0.11 // indirect
	github.com/pkg/errors v0.8.1 // indirect
)
