package main

import (
	_ "embed"
	"flag"
	"fmt"
	"log"
	"os"
	"pdf-fact/constants"
	"time"

	"github.com/magiconair/properties"
	"github.com/signintech/gopdf"
)

//go:embed configuration.properties
var configurationData []byte

type Config struct {
	identifier        string
	date              string
	dayCount          float64
	dailyRate         float64
	vat               float64
	period            string
	configurationPath string
	target            string
}

func getConfig() Config {
	identifier := flag.String("identifier", "2022-009", "invoice identifier")
	date := flag.String("date", "30/09/2022", "invoice date")
	dayCount := flag.Float64("days", 18, "number of days")
	dailyRate := flag.Float64("rate", 100, "daily rate")
	vat := flag.Float64("vat", 0.2, "value added tax")
	period := flag.String("period", "09/2022", "invoice period")
	configurationPath := flag.String("config", "", "configuration file (optional)")

	flag.Usage = usage
	flag.Parse()
	if len(os.Args) < 12 {
		usage()
		os.Exit(1)
	}

	return Config{*identifier, *date, *dayCount, *dailyRate, *vat, *period, *configurationPath, flag.Arg(0)}
}

func usage() {
	fmt.Printf("This tool builds an invoice document as PDF.\n")
	fmt.Printf("\n")

	fmt.Printf("Usage:\n")
	fmt.Printf("  invoice [options] <output file>\n")

	fmt.Printf("\n")
	fmt.Printf("Options:\n")
	fmt.Printf("  --identifier <ID>\t\tInvoice identifier\n")
	fmt.Printf("  --date <DATE>\t\t\tInvoice date\n")
	fmt.Printf("  --days <DAYS>\t\t\tNumber of days worked\n")
	fmt.Printf("  --rate <RATE>\t\t\tDaily rate\n")
	fmt.Printf("  --vat <VALUE>\t\t\tVAT\n")
	fmt.Printf("  --period <PERIOD>\t\tInvoice period\n")
	fmt.Printf("  [--config <FILE_NAME>]\tConfiguration file\n")
	fmt.Printf("\n")
}

func main() {
	config := getConfig()

	date, err := time.Parse("02/01/2006", config.date) // format derived from use 2 January 2006 at 3:04pm
	if err != nil {
		log.Fatalf("Unable to parse the date %s (expected format DD/MM/YYYY): %v", config.date, err.Error())
	}
	period, err := time.Parse("01/2006", config.period) // format derived from use 2 January 2006 at 3:04pm
	if err != nil {
		log.Fatalf("Unable to parse the period %s (expected format MM/YYYY): %v", config.period, err.Error())
	}

	configuration, _ := properties.Load(configurationData, properties.UTF8)

	if config.configurationPath != "" {
		customConfiguration, _ := properties.LoadFile(config.configurationPath, properties.UTF8)
		configuration.Merge(customConfiguration)
	}

	companyName := configuration.MustGetString("companyName")
	companyFounder := configuration.MustGetString("companyFounder")
	companyAddress := configuration.MustGetString("companyAddress")
	companyZipcode := configuration.MustGetString("companyZipcode")
	companyCity := configuration.MustGetString("companyCity")
	companyLegalMention := configuration.MustGetString("companyLegalMention")
	companyVat := configuration.MustGetString("companyVat")

	clientName := configuration.MustGetString("clientName")
	clientAddress := configuration.MustGetString("clientAddress")
	clientZipcode := configuration.MustGetString("clientZipcode")
	clientCity := configuration.MustGetString("clientCity")

	pdf := gopdf.GoPdf{}

	var textWidth float64 = gopdf.PageSizeA4.W/constants.MM2PX - 2

	// const lineHeight = fontHeight * 1.2He
	var box = gopdf.Box{Left: constants.MM2PX, Top: constants.MM2PX, Right: 595 - constants.MM2PX, Bottom: 842 - constants.MM2PX}

	// Base trim-box
	pdf.Start(gopdf.Config{
		PageSize: *gopdf.PageSizeA4, //595.28, 841.89 = A4
		TrimBox:  box,
	})

	// Page trim-box
	opt := gopdf.PageOption{
		PageSize: gopdf.PageSizeA4, //595.28, 841.89 = A4
		TrimBox:  &box,
	}
	pdf.AddPageWithOption(opt)

	normalFontPath := configuration.MustGetString("normalFontPath")
	err = pdf.AddTTFFont(constants.NORMAL_FONT, normalFontPath)
	if err != nil {
		log.Fatalf("Error while adding font %s: %v", normalFontPath, err.Error())
	}
	boldFontPath := configuration.MustGetString("boldFontPath")
	err = pdf.AddTTFFont(constants.BOLD_FONT, boldFontPath)
	if err != nil {
		log.Fatalf("Error while adding font %s: %v", boldFontPath, err.Error())
	}

	// header
	useBoldFont(&pdf, constants.FONT_HEIGHT)
	addText(&pdf, companyName, 2, 2)
	addText(&pdf, companyFounder, 2, 2.6)

	addText(&pdf, clientName, 13, 5.8)

	addText(&pdf, configuration.MustGetString("stringIssuingDate"), 2, 12)
	addText(&pdf, fmt.Sprintf("%02d/%02d/%04d", date.Day(), date.Month(), date.Year()), 8, 12)

	useBoldFont(&pdf, constants.FONT_HEIGHT*1.5)
	addText(&pdf, configuration.MustGetString("stringInvoiceIdentifier"), 2, 10)
	addText(&pdf, config.identifier, 8, 10)

	useNormalFont(&pdf, constants.FONT_HEIGHT)
	addText(&pdf, companyAddress, 2, 3.2)
	addText(&pdf, companyZipcode+" "+companyCity, 2, 3.8)
	addText(&pdf, clientAddress, 13, 6.4)
	addText(&pdf, clientZipcode+" "+clientCity, 13, 7)

	addText(&pdf, configuration.MustGetString("stringClientReference"), 13, 5)
	pdf.SetLineWidth(1)
	pdf.Line(constants.MM2PX*13, constants.MM2PX*5.45, constants.MM2PX*17, constants.MM2PX*5.45)

	// footer
	useNormalFont(&pdf, constants.FONT_HEIGHT*0.7)
	addCenteredText(&pdf, companyName+" – "+companyAddress+" "+companyZipcode+" "+companyCity, 2, 34, textWidth)
	addCenteredText(&pdf, companyLegalMention, 2, 34.6, textWidth)
	useBoldFont(&pdf, constants.FONT_HEIGHT*0.7)
	addCenteredText(&pdf, companyVat, 2, 35.2, textWidth)

	// Array
	useBoldFont(&pdf, constants.FONT_HEIGHT*0.9)
	setPosition(&pdf, 2, 14)
	addCenteredCell(&pdf, configuration.MustGetString("stringLabel"), 11, 0.7)
	addCenteredCell(&pdf, configuration.MustGetString("stringQuantity"), 2, 0.7)
	addCenteredCell(&pdf, configuration.MustGetString("stringUnitPrice"), 4, 0.7)
	addCenteredCell(&pdf, configuration.MustGetString("stringGrossAmount"), 4, 0.7)

	useNormalFont(&pdf, constants.FONT_HEIGHT*0.9)
	setPosition(&pdf, 2, 14.7)
	addCenteredCell(&pdf, "", 11, 10)
	addCenteredCell(&pdf, "", 2, 10)
	addCenteredCell(&pdf, "", 4, 10)
	addCenteredCell(&pdf, "", 4, 10)

	setPosition(&pdf, 2, 14.7)
	addNoBorderCell(&pdf, " "+configuration.MustGetString("stringServiceLabel")+" "+configuration.MustGetString("clientServiceDescription"), 11, 2, gopdf.Left|gopdf.Middle)
	amount := config.dayCount * config.dailyRate
	vat := amount * config.vat
	addNoBorderCell(&pdf, format(config.dayCount, 1)+" ", 2, 2, gopdf.Right|gopdf.Middle)
	addNoBorderCell(&pdf, format(config.dailyRate, 2)+" ", 4, 2, gopdf.Right|gopdf.Middle)
	addNoBorderCell(&pdf, format(amount, 2)+" ", 4, 2, gopdf.Right|gopdf.Middle)
	setPosition(&pdf, 2, 15.3)
	addNoBorderCell(&pdf, " "+configuration.MustGetString("clientServiceDescription2"), 11, 2, gopdf.Left|gopdf.Middle)
	setPosition(&pdf, 2, 15.9)
	addNoBorderCell(&pdf, " "+configuration.MustGetString("stringContractReference")+" "+configuration.MustGetString("clientContractReference"), 11, 2, gopdf.Left|gopdf.Middle)

	useBoldFont(&pdf, constants.FONT_HEIGHT*0.9)
	setPosition(&pdf, 13, 25.5)
	addCenteredCell(&pdf, "", 6, 1.8)
	addCenteredCell(&pdf, "", 4, 1.8)
	setPosition(&pdf, 13, 25.5)
	addNoBorderCell(&pdf, " "+configuration.MustGetString("stringEuroGrossAmount"), 6, 0.6, gopdf.Middle)
	addNoBorderCell(&pdf, format(amount, 2)+" ", 4, 0.6, gopdf.Right|gopdf.Middle)
	if config.vat != 0 {
		setPosition(&pdf, 13, 26.1)
		addNoBorderCell(&pdf, " "+configuration.MustGetString("stringVat")+" "+format(100.0*config.vat, 0)+"%", 6, 0.6, gopdf.Middle)
		addNoBorderCell(&pdf, format(vat, 2)+" ", 4, 0.6, gopdf.Right|gopdf.Middle)
	}
	setPosition(&pdf, 13, 26.7)
	addNoBorderCell(&pdf, " "+configuration.MustGetString("stringEuroNetAmount"), 6, 0.6, gopdf.Middle)
	addNoBorderCell(&pdf, format(amount+vat, 2)+" ", 4, 0.6, gopdf.Right|gopdf.Middle)

	useBoldFont(&pdf, constants.FONT_HEIGHT)
	setPosition(&pdf, 13, 28)
	addCell(&pdf, " "+configuration.MustGetString("stringToBePaid"), 6, 0.7, gopdf.Middle)
	addCell(&pdf, format(amount+vat, 2)+" ", 4, 0.7, gopdf.Right|gopdf.Middle)

	firstOfMonth := time.Date(period.Year(), period.Month(), 1, 0, 0, 0, 0, period.Location())
	lastOfMonth := firstOfMonth.AddDate(0, 1, -1)

	useNormalFont(&pdf, constants.FONT_HEIGHT*0.9)
	setPosition(&pdf, 2, 30)
	addCenteredCell(&pdf, "", 21, 3)
	addText(&pdf, " "+configuration.MustGetString("stringPeriod")+fmt.Sprintf("%02d/%02d/%04d", firstOfMonth.Day(), firstOfMonth.Month(), firstOfMonth.Year())+" au "+fmt.Sprintf("%02d/%02d/%04d", lastOfMonth.Day(), lastOfMonth.Month(), lastOfMonth.Year()), 2, 31)
	addText(&pdf, " "+configuration.MustGetString("stringVatOnCollection"), 2, 32)

	err = pdf.WritePdf(config.target)
	if err != nil {
		log.Fatalf("Error while writing PDF %s: %v", config.target, err.Error())
	}
}

func format(f float64, width int) string {
	if width == 1 {
		return intComma(int(f)) + "," + fmt.Sprintf("%01d", int(f*10.0)%10)
	}
	if width == 2 {
		return intComma(int(f)) + "," + fmt.Sprintf("%02d", int(f*100.0)%100)
	}
	return intComma(int(f))
}

func intComma(i int) string {
	if i < 0 {
		return "-" + intComma(-i)
	}
	if i < 1000 {
		return fmt.Sprintf("%d", i)
	}
	return intComma(i/1000) + " " + fmt.Sprintf("%03d", i%1000)
}

func useNormalFont(pdf *gopdf.GoPdf, size interface{}) {
	family := constants.NORMAL_FONT
	err := pdf.SetFont(family, "", size)
	if err != nil {
		log.Fatalf("Error while setting font %s with size %v: %v", family, size, err.Error())
	}
}

func useBoldFont(pdf *gopdf.GoPdf, size interface{}) {
	family := constants.BOLD_FONT
	err := pdf.SetFont(family, "", size)
	if err != nil {
		log.Fatalf("Error while setting font %s with size %v: %v", family, size, err.Error())
	}
}

func setPosition(pdf *gopdf.GoPdf,
	x, y float64,
) {
	pdf.SetXY(constants.MM2PX*x, constants.MM2PX*y)
}

func addText(pdf *gopdf.GoPdf,
	text string,
	x, y float64,
) {
	pdf.SetXY(constants.MM2PX*x, constants.MM2PX*y)
	err := pdf.Cell(nil, text)
	if err != nil {
		log.Fatalf("Error while adding cell: %v", err.Error())
	}
}

func addCell(pdf *gopdf.GoPdf,
	text string,
	w, h float64,
	align int,
) {
	err := pdf.CellWithOption(&gopdf.Rect{W: constants.MM2PX * w, H: constants.MM2PX * h},
		text,
		gopdf.CellOption{Align: align, Border: gopdf.Left | gopdf.Right | gopdf.Top | gopdf.Bottom})
	if err != nil {
		log.Fatalf("Error while adding cell: %v", err.Error())
	}
}

func addNoBorderCell(pdf *gopdf.GoPdf,
	text string,
	w, h float64,
	align int,
) {
	err := pdf.CellWithOption(&gopdf.Rect{W: constants.MM2PX * w, H: constants.MM2PX * h},
		text,
		gopdf.CellOption{Align: align})
	if err != nil {
		log.Fatalf("Error while adding cell: %v", err.Error())
	}
}

func addCenteredCell(pdf *gopdf.GoPdf,
	text string,
	w, h float64,
) {
	err := pdf.CellWithOption(&gopdf.Rect{W: constants.MM2PX * w, H: constants.MM2PX * h},
		text,
		gopdf.CellOption{Align: gopdf.Center | gopdf.Middle, Border: gopdf.Left | gopdf.Right | gopdf.Top | gopdf.Bottom})
	if err != nil {
		log.Fatalf("Error while adding cell: %v", err.Error())
	}
}

func addCenteredText(pdf *gopdf.GoPdf,
	text string,
	x, y, w float64,
) {
	pdf.SetXY(constants.MM2PX*x, constants.MM2PX*y)
	err := pdf.CellWithOption(&gopdf.Rect{W: constants.MM2PX * w, H: 40},
		text,
		gopdf.CellOption{Align: gopdf.Center})
	if err != nil {
		log.Fatalf("Error while adding cell: %v", err.Error())
	}
}
