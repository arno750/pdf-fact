package constants

var (
	NORMAL_FONT = "normal"
	BOLD_FONT   = "bold"

	MM2PX       = 22.68
	FONT_HEIGHT = 12.0
)
