.DEFAULT_GOAL := build
.PHONY: help echo build lint test buildoptim install run clean all

PACKAGE_NAME ?= pdf-fact
VERSION ?= $(shell git describe --tags)

help: ## This help message
	@echo "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\033[1;34m\1\\033[0;39m:\2/' | column -c2 -t -s :)"

format: ## Format code with gofmt
	gofmt -w .

echo: ## Display the package name and version
	@echo ${PACKAGE_NAME}-${VERSION}

build: ## Build the application
	go build

lint: ## Lint the application
	docker run --rm -it -w "/app" -v "${PWD}":/app golangci/golangci-lint:latest golangci-lint run -v

test: ## Run non-regression test
	go test -cover ./...

buildoptim: ## Build the application
	go build -ldflags="-s -w"

install: build ## Install the application
	mv ${PACKAGE_NAME} ~/opt/bin/.

run: build ## Run the application
	./${PACKAGE_NAME} --identifier "2022-009" --date "03/10/2022" --days "18.0" --rate "200.00" --period "09/2022" test.pdf

clean: ## Clean
	rm -f ${PACKAGE_NAME}

all: test build install
