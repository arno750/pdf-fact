package main

import (
	_ "embed"
	_ "unsafe"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

//go:linkname icons fyne.io/fyne/v2/theme.icons
var icons map[fyne.ThemeIconName]fyne.Resource

var (
	//go:embed icons.go
	src_icons string
)

func main() {
	app := app.New()

	w := app.NewWindow("Fyne icons browser")

	w.SetContent(iconScreen(w))
	w.Resize(fyne.NewSize(640, 460))
	w.ShowAndRun()
}

func iconScreen(_ fyne.Window) fyne.CanvasObject {
	txtSource := widget.NewMultiLineEntry()
	txtSource.SetText(src_icons)

	txt := widget.NewEntry()
	c := container.NewGridWrap(fyne.Size{Width: 50, Height: 50})
	cc := container.NewVScroll(container.NewVBox(txt, c))

	for _, icon := range icons {
		btn := widget.NewButtonWithIcon("", icon, nil)
		btn.OnTapped = func() {
			txt.SetText(btn.Icon.Name())
		}

		c.Add(btn)
	}

	tabs := container.NewAppTabs(
		container.NewTabItem("main", cc),
		container.NewTabItem("source", txtSource),
	)
	tabs.SetTabLocation(container.TabLocationBottom)
	return tabs
}
